# Copyright (C) 2015-2016  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

#!/usr/bin/env python3
# coding: utf-8

import os
import sys
import io
import json

def main(root):
    
    root_ground_truth = root + '/../ground_truth'
    root_ground_truth_text = root_ground_truth + '_text'
    root_code_by_language = root + '/../code_by_language'
    counts = dict()
    
    try:
        os.mkdir(root_code_by_language)
    except FileExistsError:
        pass
    
    for r in os.listdir(root):
        if not r.startswith('.'):
            for d in os.listdir(root + '/' + r):
                if not d.startswith('.'):
                    try:
                        ground_truth = io.open(root_ground_truth + '/' + r + '/' + d + '.json')
                        try:
                            j = json.load(ground_truth)
                            for language in j.keys():
                                root_language = root_code_by_language + '/' + language
                                try:
                                    os.mkdir(root_language)
                                except FileExistsError:
                                    pass
                                for f in j.get(language):
                                    copy_src = root + '/' + r + '/' + d + '/' + f
                                    try:
                                        if os.path.getsize(copy_src) > 10485760 :
                                            continue
                                    except FileNotFoundError:
                                        continue
                                    counts[language] = counts.get(language, 0) + 1
                                    start = (counts[language] - 1) // 1000 * 1000 + 1
                                    end = start + 999
                                    root_count = root_language + '/' + str(start) + '-' + str(end)
                                    if counts[language] % 1000 == 1:
                                        try: 
                                            os.mkdir(root_count)
                                        except FileExistsError:
                                            pass
                                    (_,ext) = os.path.splitext(f)
                                    new_name = str(counts[language]) + ext
                                   
                                    copy_des = root_count + '/' + new_name
                                    try:
                                        os.symlink(copy_src, copy_des)
                                        print('{} successfully copied.'.format(copy_src))
                                    except FileExistsError:
                                        pass
                        except json.decoder.JSONDecodeError:
                            ground_truth.close()
                            ground_truth = io.open(root_ground_truth_text + '/' + r + '/' + d, 'r')
                            while(True):
                                line = ground_truth.readline()
                                if line == '\n' or line == '':
                                    break
                                else:
                                    pass
                                
                            while(True):
                                line = ground_truth.readline()
                                stripped = line.strip()
                                if line == '':
                                    break
                                else:
                                    stripped = line.strip()
                                    language = stripped.replace(':','')
                                    root_language = root_code_by_language + '/' + language
                                    try:
                                        os.mkdir(root_language)
                                    except FileExistsError:
                                        pass
                                    while(True):
                                        line = ground_truth.readline()
                                        if line == '\n':
                                            break
                                        else:
                                            copy_src = root + '/' + r + '/' + d + '/' + f
                                            try:
                                                if os.path.getsize(copy_src) > 10485760 :
                                                    continue
                                            except FileNotFoundError:
                                                continue
                                            counts[language] = counts.get(language, 0) + 1
                                            start = (counts[language] - 1) // 1000 * 1000 + 1
                                            end = start + 999
                                            root_count = root_language + '/' + str(start) + '-' + str(end)
                                            if counts[language] % 1000 == 1:
                                                try: 
                                                    os.mkdir(root_count)
                                                except FileExistsError:
                                                    pass
                                            (_,ext) = os.path.splitext(f)
                                            new_name = str(counts[language]) + ext
                                            copy_des = root_count + '/' + new_name
                                            try:
                                                os.symlink(copy_src, copy_des)
                                                print('{} successfully copied.'.format(copy_src))
                                            except FileExistsError:
                                                pass
                    finally:
                        ground_truth.close()
                            
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Only argument acceptable is a path.')
    else:
        main(sys.argv[1])
