#!/bin/bash

# This script uses linguist to get ground truth of source code identity.

ROOT_FOLDER=$1
OLDIFS=$IFS
IFS=$'\n'

if [ $# -eq 1 ]
then
    mkdir $ROOT_FOLDER/../ground_truth/;
    cd /Users/yinyuan/Desktop/TRE/linguist;
    
    for root in $( ls $ROOT_FOLDER -1 );
    do
        mkdir "$ROOT_FOLDER/../ground_truth/$root/";
	for repo in $( ls "$ROOT_FOLDER/$root/" -1 );
	do
	    echo "Generating ground truth of '$repo'";
	    bundle exec linguist "$ROOT_FOLDER/$root/$repo" --json > "$ROOT_FOLDER/../ground_truth/$root/$repo.json";
	done
    done
else
    echo "Please enter root folder correctly. One folder is needed."
fi

IFS=$OLDIFS
