#!/bin/bash/python3

import sys, os
from pickle import load
from collections import namedtuple, Counter
try:
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MaxNLocator
except ImportError:
    raise ImportError('Please run `pip3 install matplotlib\' in command line.')

def heatmap(path, name):
    with open(path, 'rb') as f:
        data = load(f)
    mat = process(data)
    labels = sorted(data)

    fig, ax = plt.subplots()
    fig.set_size_inches(0.25 * len(labels), 0.25 * len(labels))
    heatmap = ax.matshow(mat, cmap='Blues')

    fig = plt.gcf()
    ax.set_frame_on(False)
    
    ax.set_yticks(np.arange(len(labels)), minor=False)
    ax.set_xticks(np.arange(len(labels)), minor=False)

    ax.set_xlabel('Classification of test files')
    ax.set_ylabel('Ground truth class of test files')
    ax.set_xticklabels(labels, minor=False)
    ax.set_yticklabels(labels, minor=False)
    ax.xaxis.tick_top()
    ax.xaxis.set_label_position('top')
    plt.xticks(rotation=90)
    ax.grid(False)

    '''
    for i in np.arange(len(mat)):
        for j in np.arange(len(mat[i])):
            ax.text(i, j, "%.1f" % (mat[i][j] * 100), color='white')
    '''

    ax = plt.gca()

    for t in ax.xaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False
    for t in ax.yaxis.get_major_ticks():
        t.tick1On = False
        t.tick2On = False

    fig.savefig("{}.pdf".format(name), bbox_inches='tight')
    
def process(data):
    '''
    '''
    ldata = sorted(data)
    length = len(ldata)
    out = [[0 for x in range(length)] for y in range(length)]
    for lang in ldata:
        index_lan = ldata.index(lang)
        ok = data[lang][0]
        if data[lang][1] > 1000 :
            test_size = 1000
        else:
            test_size = data[lang][1]
        result = [x[1] for x in data[lang][3]]
        counter = dict(Counter(result))
        for res_lan in counter.keys():
            index_res = ldata.index(res_lan)
            out[index_lan][index_res] = counter.get(res_lan, 0) / test_size
            
    return out

def get_recall(data):
    ldata = sorted(data)
    out = {}
    true_pos = 0
    false_neg = 0
    for lang in ldata:
        ok = data[lang][0]
        if data[lang][1] > 1000:
            test_size = 1000
        else:
            test_size = data[lang][1]
        result = [x[1] for x in data[lang][3]]
        counter = dict(Counter(result))
        out[lang] = counter.get(lang, 0) / test_size

        true_pos += counter.get(lang,0)
        false_neg += test_size - counter.get(lang,0)
    print(true_pos)
    print(false_neg)
    print(true_pos / (true_pos + false_neg))
    return out

def get_precision(data):
    ldata = sorted(data)
    out = {}
    true_pos = 0
    false_pos = 0
    a = []
    for lang in ldata:
        a += [x[1] for x in data[lang][3]]
    counter_all = dict(Counter(a))

    for lang in ldata:
        ok = data[lang][0]
        if data[lang][1] > 1000:
            test_size = 1000
        else:
            test_size = data[lang][1]
        result = [x[1] for x in data[lang][3]]
        counter = dict(Counter(result))
        try:
            out[lang] = counter.get(lang, 0) / counter_all.get(lang, 0)
        except ZeroDivisionError:
            out[lang] = 0

        true_pos += counter.get(lang,0)
        false_pos += counter_all.get(lang,0) - counter.get(lang,0)
    return out

def get_f1(recalls, precisions):
    f1 = {}
    for k in list(recalls.keys()):
        recall = recalls[k]
        precision = precisions[k]
        f1[k] = harmonic_mean([recall, precision])
    return f1

def harmonic_mean(l):
    l_pos = [x for x in l if x > 0]
    H_T = len(l)
    H_0 = H_T - len(l_pos)
    if H_T == H_0:
        return 0
    else:
        return ((H_T - H_0) / sum([1 / x for x in l_pos])) * ((H_T - H_0) / H_T)
        

def compare(results, suffix):
    
    datas = []
    for result in results:
        with open(result, 'rb') as f:
            datas.append(load(f))
            
    dicts = []
    for data in datas:
        recalls = get_recall(data)
        precisions = get_precision(data)
        dicts.append(get_f1(recalls, precisions))
        
    if len(results) == 1:
        import operator
        all_lang = [ x[0] for x in sorted(dicts[0].items(), key=operator.itemgetter(1))][:25]
    else:
        s = set(dicts[0].keys())
        for d in dicts:
            s &= set(d.keys())
        all_lang = sorted(list(s))[::-1]
        # all_lang = sorted(list(set().union(dicts[0].keys(),dicts[1].keys())))[::-1]
        
    n = len(all_lang)

    accs = []
    for d in dicts:
        accs.append([d.get(lang, 0) for lang in all_lang])
    
    fig, ax = plt.subplots()
    fig.set_size_inches(15, 0.2 * len(results) * n)
    ind = np.arange(n)
    width = 0.75 / len(results)
    opacity = 0.4

    rectss = []
    colors = ['b', 'r', 'c', 'm', 'y', 'g']
    
    for idx, result in enumerate(results):
        rectss.append(ax.barh(ind - (idx - len(results) / 2) * width, accs[idx], width, alpha=opacity, color=colors[idx % len(colors)], label=os.path.basename(result)))
    ax.set_xlabel('F1 score / %')
    ax.set_yticks(ind + width / 2)
    ax.set_yticklabels(all_lang)
    ax.set_xlim(xmin=0, xmax=1)
    vals = ax.get_xticks()
    ax.set_xticklabels(['{:3.0f}%'.format(x * 100) for x in vals])
    ax.xaxis.tick_top()
    #if len(rectss) > 1:
    ax.legend()

    def autolabel(rects):
        for rect in rects:
            width = rect.get_width()
            ax.text(width + 0.01, rect.get_y() + rect.get_height() / 2., '{0:.1f}%'.format(width * 100), ha='left', va='center')

    for rects in rectss:
        autolabel(rects)
    plt.ylim([-1,n+1])

    fig.tight_layout()
    fig.savefig("comparison_{}.pdf".format(suffix), bbox_inches='tight')

if __name__ == '__main__':
    if len(sys.argv) == 4 and sys.argv[1] == '--cm':
        heatmap(sys.argv[2], sys.argv[-1])
    elif len(sys.argv) >= 4 and sys.argv[1] == '--f1':
        compare(sys.argv[2:-1], sys.argv[-1])
    else:
        print('Please check arguments.')
