#!/usr/bin/python

from PyQt5              import QtGui, QtCore
from pyforms 	        import BaseWidget
from pyforms.controls 	import ControlTextArea
from pyforms.controls 	import ControlDir
from pyforms.controls 	import ControlList
from pyforms.controls 	import ControlLabel
from pyforms.controls 	import ControlCombo
from .cnn               import CNN

import pyforms, os, gzip
from pickle import load, dump

RED   = QtGui.QColor(255,0,0)
WHITE = QtGui.QColor(255,255,255)
GREEN = QtGui.QColor(0,255,0)
BLACK = QtGui.QColor(0,0,0)

class Check(BaseWidget):
    
    def __init__(self):
        super(Check, self).__init__('Software Heritage Source Code Language Manual Check Tool')

        self._control_root = ControlDir('Choose the root of database: ')
        self._control = ControlDir('Choose a directory: ')
        self._list = ControlList('Files in the directory')
        self._text = ControlTextArea('Content')
        self._label = ControlLabel('Language:            \nValue:           ')
        self._label_rest = ControlLabel('')
        self._combo = ControlCombo('Is that correct ?')
		
        self.formset = ['_control_root', '_control', ('_list', ['_text', ('_label', '_combo')]),'_label_rest']
        self._control_root.changed_event = self.__save_root
        self._control.changed_event = self.__get_files
        self._list.readonly = True
        self._list.item_selection_changed_event=self.__show_text
        self._text.readonly = True
        self._cnn = CNN(None, 4096, None)
        self._root = None

        self._dict = {}
        self._combo += ('Unknown', None)
        self._combo += ('No', False)
        self._combo += ('Yes', True)
        self._combo.activated_event = self.__checked

        self._curr_row = None
        self._curr_column = None
        self._curr_dir = None
        self._state = 0

        self.before_close_event = self.__store

    def __save_root(self):
        self._root = self._control_root.value
        try:
            with open(os.path.join(self._root, 'results'), 'rb') as f:
                self._dicts = load(f)
        except Exception:
            self._dicts = {}
        self._state = 1
        self._control.value = self._root

    def __store(self):
        with open(os.path.join(self._root, 'results'), 'wb') as f:
            self._dicts[self._curr_dir] = self._dict
            dump(self._dicts, f)

    def __get_files(self):
        if self._state == 1:
            self._state = 2
            return
        elif self._state == 0:
            self.alert_popup('Please choose root of your database.', title='Error')
            return
        res = []
        if self._curr_dir != None:
            self._dicts[self._curr_dir] = self._dict
        self._curr_dir = self._control.value
        self._dict = self._dicts.get(self._curr_dir, {})
        for root, sub, files in os.walk(self._curr_dir):
            if sub == []:
                for file in files:
                    if not file.startswith('.'):
                        res.append((os.path.join(root, file),))
        self._list.value = res
        self._update_status()
        self._update_cells_color()

    def __checked(self, index):
        path = self._list.get_value(self._curr_column, self._curr_row)
        self._dict[path] = self._combo.value
        if self._combo.value == 'Unknown':
            del self._dict[path]
        self._update_color(self._combo.value, self._list.get_cell(self._curr_column, self._curr_row))
        self._update_status()
        
    def _update_status(self):
        correct   = len([x for x in self._dict.values() if x == True])
        wrong     = len(self._dict.keys()) - correct
        remaining = len(self._list.value) - len(self._dict.keys())
        try:
            accuracy  = correct / (correct + wrong) * 100
        except:
            accuracy  = 0
        self._label_rest.value = 'Correct:\t{}\tWrong:\t{}\tRemaining:\t{}\tAccuracy:\t{:.2f}%'.format(correct, wrong, remaining, accuracy)

    def _update_cells_color(self):
        n = self._list.rows_count
        for i in range(0, n):
            cell = self._list.get_cell(0, i)
            value = self._list.get_value(0, i)
            self._update_color(self._dict.get(value, None), cell)

    def _update_color(self, x, cell):
        if x == False:
            cell.setBackground(RED)
        elif x == True:
            cell.setBackground(GREEN)
        else:
            cell.setBackground(WHITE)

    def __show_text(self):
        column = 0
        row = self._list.selected_row_index
        self._curr_row = row
        self._curr_column = column
        if row == None:
            self._text.value = ''
            self._label.value = 'Language:            \nValue:           '
            return
            
        path = self._list.get_value(column, row)
        with gzip.open(path, 'rb') as f:
            string = f.read()
        try:
            string = string.decode('utf-8')
        except UnicodeDecodeError:
            pass
            
        self._text.value = string[:10240]
        res = self._cnn.classify(path)
        if(res[1] >= 0):
            self._label.value = 'Language: {}\nValue: {}'.format(res[0],res[1])
        else:
            self._label.value = 'Language: No Reliable Result\nValue:           '

        h_sel = self._dict.get(path, None)
        if h_sel == None:
            self._combo.current_index = 0
        elif h_sel == False:
            self._combo.current_index = 1
        elif h_sel == True:
            self._combo.current_index = 2
            

#Execute the application
if __name__ == "__main__":
    pyforms.start_app(Check)
