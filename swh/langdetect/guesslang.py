
import os, sys, subprocess, time
import numpy as np
import tensorflow as tf

from itertools import islice
from pickle import dump, load
from collections import Counter
from numpy import array
from .utils.common import tokenizer, file_to_string, find_file, count_files
from keras.preprocessing.sequence import pad_sequences
from keras.utils import np_utils

class Guesslang:

    def __init__(self, root):
        # Root of dataset
        self._root = root

        # Root of training set
        self._root_training_set = os.path.join(self._root, '..', 'training_set')

        # Root of model folder
        self._root_model = os.path.join(self._root, '..', 'model_guesslang')

        # Root of arranged dataset
        self._root_language_dataset = os.path.join(self._root, '..', 'code_by_language')

        # Path of result
        self._path_result = os.path.join(self._root, '..', 'result_guesslang')

        self.languages = [x for x in os.listdir(self._root_training_set) if not x.startswith('.')]
        self.LENGTH = 1000
        self.TOTAL_CLASS = len(self.languages)
                
        feature_columns = [tf.contrib.layers.real_valued_column('', dimension=self.LENGTH)]
        
        self._classifer = tf.contrib.learn.DNNLinearCombinedClassifier(
            linear_feature_columns=feature_columns,
            dnn_feature_columns=feature_columns,
            dnn_hidden_units=[256, 64, 16],
            n_classes=self.TOTAL_CLASS,
            linear_optimizer=tf.train.RMSPropOptimizer(0.05),
            dnn_optimizer=tf.train.RMSPropOptimizer(0.05),
            model_dir=self._root_model,
            fix_global_step_increment_bug=True
        )

    def train(self):
        try:
            if len(os.listdir(self._root_training_set)) == 0:
                build_training_set(self._root)
            try:
                os.mkdir(self._root_model)
            except FileExistsError:
                pass
        except FileNotFoundError:
            os.mkdir(self._root_training_set)
            build_training_set(self._root)

        try:
            f = open(os.path.join(self._root, '..', 'model_guesslang', 'texts+labels'), 'rb')
            train_file_with_label = load(f)
        except FileNotFoundError:
            train_file_with_label = self._train_file_with_label()
            with open(os.path.join(self._root, '..', 'model_guesslang', 'texts+labels'), 'wb') as f:
                dump(train_file_with_label, f)

        for index in range(self.TOTAL_CLASS):
            self._classifer.partial_fit(input_fn=lambda:self._generator(self.LENGTH, self.TOTAL_CLASS, index),steps=500)

    def _generator(self, length, total_class, index):
        print("Language: {}".format(index))
        with open(os.path.join(self._root, '..', 'model_guesslang', 'texts+labels'), 'rb') as f:
            train_file_with_label = load(f)
            X = np.empty((0, length))
            Y = np.empty((0, 1), dtype=int)
            for path, label in train_file_with_label:
                if label == index:
                    X = np.append(X, self._file_to_x(path), axis=0)
                    l = array([label], dtype=int)
                    Y = np.append(Y, l)
            return tf.convert_to_tensor(X), tf.convert_to_tensor(Y)

    def _file_to_x(self, filename):
        wrapper = (lambda x: x + 1)
        tokens = [wrapper(x) for x in tokenizer(file_to_string(filename), 'letter')]
        return pad_sequences([tokens], maxlen=self.LENGTH)
                
    def _train_file_with_label(self):
        l = []
        
        for language in self.languages:
            root_training_set_language = os.path.join(self._root_training_set, language)
            root_stat_language = os.path.join(self._root_model, language)
            index_lang = self.languages.index(language)
            if os.path.isfile(root_stat_language):
                continue
            print(language)
            for f in [x for x in os.listdir(root_training_set_language) if not x.startswith('.')]:
                filename = os.path.join(root_training_set_language, f)
                l.append((filename, index_lang))

        return l

    def _max_len(self, texts):
        return max([len(text) for text in texts])
                
    def _vocabulary_size(self, texts):
        vocabulary = dict(Counter([token for text in texts for token in text]))
        return len(vocabulary.keys())

    def test(self):
        try:
            r = open(self._path_result, 'rb')
            test_result = load(r)
            r.close()
        except FileNotFoundError:
            test_result = {}
        
        for language in [x for x in os.listdir(self._root_training_set) if not x.startswith('.') and x not in test_result.keys()]:
            test_result[language] = self.test_class(language)
            with open(self._path_result, 'wb') as f:
                dump(test_result, f)

    def _get_test_set(self, language):
        root_training_language = os.path.join(self._root_training_set, language)
        root_language = os.path.join(self._root_language_dataset, language)
        total = count_files(root_language)
        training_set = [int(os.path.splitext(x)[0]) for x in os.listdir(root_training_language) if not x.startswith('.')]
        it = (find_file(root_language, x) for x in range(1, total + 1) if x not in training_set and os.path.getsize(find_file(root_language, x)) <= 1048576)
        test_set = list(islice(it, 1000))
        if len(test_set) == 0:
            it = (find_file(root_language, x) for x in range(1, total + 1) if x not in training_set)
            test_set = list(islice(it, 1000))
        return test_set
    
    def _count_size(self, files):
        size = 0
        for f in files:
            size += os.path.getsize(f)
        return size
    
    def test_class(self, language):
        test_set = self._get_test_set(language)

        ok = 0
        results = []
        count = 0
        length = len(test_set)
        for test in test_set:
            result = self._guess_file_language(test)
            count += 1
            print('[{0:4d}/{1:4d}] {2}       '.format(count, length, result),end='\r')
            results.append(result)
            if result == language:
                ok += 1

        total_test = len(test_set)
        accuracy = ok / len(test_set)
        print('Tests for {}                   '.format(language))
        print('Total test files           : {}'.format(total_test))
        print('Correctly classified files : {}'.format(ok))
        print('Accuracy                   : {}%'.format(accuracy * 100))
        return (ok, len(test_set), accuracy, results)

    def speed_benchmark(self):
        language = [x for x in os.listdir(self._root_model) if not x.startswith('.')][10]
        models = self._load_models()

        test_set = self._get_test_set(language)
        total_size = self._count_size(test_set)
        print('{} kB in total'.format(total_size / 1024))
        
        t_start = time.perf_counter()
        self.test_class(models, language)
        t_end = time.perf_counter()
        
        print('{} seconds.'.format(t_end - t_start))
        print('{} seconds per kB'.format(((t_end - t_start) / total_size) * 1024))

    def _guess_file_language(self, filename):
        x = self._file_to_x(filename)

        result = list(self._classifer.predict(x=x))[0]
        return self.languages[result]

if __name__ == '__main__':
    if len(sys.argv) == 3 and sys.argv[1] == '--train':
        n = Guesslang(sys.argv[2])
        n.train()
    elif len(sys.argv) == 3 and sys.argv[1] == '--test':
        n = Guesslang(sys.argv[2])
        n.test()
    elif len(sys.argv) == 3 and sys.argv[1] == '--benchmark':
        n = NGramProb(sys.argv[2])
        n.speed_benchmark()
    elif len(sys.argv) == 4 and sys.argv[1] == '--test':
        n = NGramProb(sys.argv[2])
        n.test_class(n.load_models(), sys.argv[3])
    else:
        print('Wrong arguments, please check your input.')
