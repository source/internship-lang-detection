
import os, sys, subprocess, time, csv, argparse, json
import kenlm

from ast import literal_eval
from itertools import islice
from pickle import dump, load
from .utils.common import Tokenizer, file_to_string, find_file, count_files, remove_comment

csv.field_size_limit(sys.maxsize)

def main():
    parser = argparse.ArgumentParser(description='Training and test tool of n-grams model.')

    subparsers = parser.add_subparsers(dest='sub_command')

    parser_train = subparsers.add_parser('train', help='Training on the dataset, dataset must be a *.csv file. A model will be created in the same directory.')
    parser_train.add_argument('train_path', metavar='PATH', type=str, help='Path of the training dataset.')
    # parser_train.add_argument('-n', '--ngrams',  metavar='N', dest='train_maxsize', type=int, help='Set maximum input size of ConvNet, default 5.')
    parser_test = subparsers.add_parser('test', help='Test on the dataset, dataset must be a directory with *.csv dataset named by corresponding language.')
    parser_test.add_argument('test_root', metavar='ROOT', type=str, help='Root of the test dataset.')
    
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()
    args = parser.parse_args()
    
    if args.sub_command == 'train' :
        n = NGramProb(args.train_path)
        n.train()
    elif args.sub_command == 'test':
        n = NGramProb(args.test_root)
        n.test()
    else:
        parser.parse_args('-h')


class NGramProb:

    def __init__(self, path):
        
        self._path = path

        # Root of model folder
        self._root_model = os.path.join(os.path.dirname(path), 'model_ngram_prob')
        try:
            os.mkdir(self._root_model)
        except:
            pass
        try:
            os.mkdir(os.path.join(self._root_model, 'arpa'))
        except:
            pass
        try:
            os.mkdir(os.path.join(self._root_model, 'text'))
        except:
            pass

        # Path of result
        self._path_result = os.path.join(os.path.dirname(path), 'result_ngram_prob')

        dir_path = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(dir_path, 'static_data', 'languages.json'), 'r') as f:
            self._languages = json.load(f)

        self._path_test_csv = path

        self._num_of_classes = len(self._languages)

    def file_len(self, fname):
        with open(fname) as f:
            count = 0
            for l in f:
                count += 1
            return count

    def train(self):
        command = [os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                '..' , '..', 'bin', 'lmplz'),
                   '-o', '3', '--discount_fallback']
        
        with open(self._path, newline='') as csvfile:
            r = csv.reader(csvfile, delimiter=' ', quotechar='|')
            label = 0
            language = self._languages[label]
            texts = []
            for pair in r:
                label_new, _ = pair
                if label != int(label_new):
                    with open(os.path.join(self._root_model, 'arpa', language), 'wb') as f:
                        train_text = ' '.join(texts)
                        with open(os.path.join(self._root_model, 'text', language), 'w') as t:
                            t.write(train_text)
                        with open(os.path.join(self._root_model, 'text', language), 'r') as t:
                            proc = subprocess.Popen(command, stdin=t, stdout=f)
                            proc.communicate()
                    texts = []
                label, string = pair
                label = int(label)
                language = self._languages[label]
                print(language, end='\r')
                
                string = literal_eval(string)
                tokens = Tokenizer.tokenize(string, 'letter')
                texts.append(' '.join(chr(token) for token in tokens))

                #tokens = Tokenizer.tokenize(string, 'word')
                #textb  = b' '.join(tokens)
                #text   = ''.join([chr(x) for x in list(textb)])
                #text   = ' '.join([x for x in text.split(' ') if x.strip('')])
                #texts.append(text)
                
            with open(os.path.join(self._root_model, 'arpa', language), 'wb') as f:
                train_text = ' '.join(texts)
                with open(os.path.join(self._root_model, 'text', language), 'w') as t:
                    t.write(train_text)
                with open(os.path.join(self._root_model, 'text', language), 'r') as t:
                    proc = subprocess.Popen(command, stdin=t, stdout=f)
                    proc.communicate()

                    

    def test(self):
        try:
            r = open(self._path_result, 'rb')
            test_result = load(r)
            r.close()
        except FileNotFoundError:
            test_result = {}
        models = self._load_models()
        
        for language in [x for x in self._languages if x not in test_result.keys()]:
            test_result[language] = self.test_class(models, language)
            with open(self._path_result, 'wb') as f:
                dump(test_result, f)
            
    def _load_models(self):
        models = {}
        
        for model in [model
                      for model in self._languages]:
            root_model = os.path.join(self._root_model, 'arpa', model)
            models[model] = kenlm.LanguageModel(root_model)
        return models
    
    def _count_size(self, files):
        size = 0
        for f in files:
            size += os.path.getsize(f)
        return size
    
    def test_class(self, model, language):
        ok = 0
        results = []
        count = 0
        total_test = self.file_len(os.path.join(self._path_test_csv, language + '.csv'))
                          
        with open(os.path.join(self._path_test_csv, language + '.csv'), newline='') as csvfile:
            r = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for pair in r:
                label, string = pair
                label = int(label)
                string = literal_eval(string)
                result = self._guess_file_language(model, string)
                count += 1
                print('[{0:4d}/{1:4d}] {2}:{3}       '.format(count, total_test, result[0][1], result[0][0]),end='\r')
                results.append(result[0])
                if result[0][1] == language:
                    ok += 1

        accuracy = ok / total_test
        print('Tests for {}                   '.format(language))
        print('Total test files           : {}'.format(total_test))
        print('Correctly classified files : {}'.format(ok))
        print('Accuracy                   : {}%'.format(accuracy * 100))
        return (ok, total_test, accuracy, results)

    def speed_benchmark(self):
        language = self._languages[10]
        model = self._load_model()

        test_set = self._get_test_set(language)
        total_size = self._count_size(test_set)
        print('{} kB in total'.format(total_size / 1024))
        
        t_start = time.perf_counter()
        self.test_class(model, language)
        t_end = time.perf_counter()
        
        print('{} seconds.'.format(t_end - t_start))
        print('{} seconds per KiB'.format(((t_end - t_start) / total_size) * 1024))

    def _guess_file_language(self, models, string):
        tokens = Tokenizer.tokenize(string, 'letter')
        text = ' '.join(chr(token) for token in tokens)

        #tokens = Tokenizer.tokenize(string, 'word')
        #textb = b' '.join(tokens)
        #text = ''.join([chr(x) for x in list(textb)])

        result = []
        
        for model_key in models.keys():
            root_model = os.path.join(self._root_model, model_key)
            model = models[model_key]
            score = model.score(text)
            result.append((score, model_key))
        return sorted(result, reverse=True)

if __name__ == '__main__':
    main()
