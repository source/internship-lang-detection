import os
import sys
import operator
import nltk
import random
import time
import numpy as np
import csv
import argparse
import json

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn.metrics as metrics

from ast import literal_eval
from itertools import islice
from pickle import dump, load
from .utils.common import Tokenizer
from nltk.util import ngrams
from collections import Counter
from sklearn.feature_extraction.text import HashingVectorizer, TfidfVectorizer
from sklearn.externals import joblib
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.cluster import DBSCAN
from sklearn.decomposition import TruncatedSVD
from scipy.sparse import vstack
from scipy.sparse import csr_matrix

csv.field_size_limit(sys.maxsize)

def main():
    parser = argparse.ArgumentParser(description='Training and test tool of multinumial naive bayesian.')

    subparsers = parser.add_subparsers(dest='sub_command')

    parser_train = subparsers.add_parser('train', help='Training on the dataset, dataset must be a *.csv file. A model will be created in the same directory.')
    parser_train.add_argument('train_path', metavar='PATH', type=str, help='Path of the training dataset.')
    # parser_train.add_argument('-n', '--ngrams',  metavar='N', dest='train_maxsize', type=int, help='Set maximum input size of ConvNet, default 5.')
    parser_test = subparsers.add_parser('test', help='Test on the dataset, dataset must be a directory with *.csv dataset named by corresponding language.')
    parser_test.add_argument('test_root', metavar='ROOT', type=str, help='Root of the test dataset.')
    
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()
    args = parser.parse_args()
    
    if args.sub_command == 'train' :
        n = Unsupervised(args.train_path)
        # n.train()
        n.graph_top_20()
    elif args.sub_command == 'test':
        n = Unsupervised(args.test_root)
        n.test()
    else:
        parser.parse_args('-h')

class Unsupervised:
    
    def __init__(self, path):
        
        self._path = path

        # Root of model folder
        self._root_model = os.path.join(os.path.dirname(path), 'model_dbscan')
        try:
            os.mkdir(self._root_model)
        except:
            pass

        # Path of result
        self._path_result = os.path.join(os.path.dirname(path), 'result_dbscan')

        dir_path = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(dir_path, 'static_data', 'languages.json'), 'r') as f:
            self._languages = json.load(f)

        self._path_test_csv = path

        self._num_of_classes = len(self._languages)
        
    def train(self):    
        cv = HashingVectorizer(analyzer='char', ngram_range=(1, 5), n_features=2**20, alternate_sign=False)
        texts = []
        label = 0
        string = ''

        top_20 = ['Python', 'Java', 'JavaScript', 'PHP', 'C#', 'C', 'C++',
                  'R', 'Objective-C', 'Swift', 'Matlab', 'Ruby', 'TypeScript',
                  'Visual Basic', 'Scala', 'Kotlin', 'Go', 'Perl', 'Lua',
                  'Rust', 'Haskell']
        top_20 = [self._languages.index(x) for x in top_20]
        print(top_20)

        with open(self._path, newline='') as csvfile:
            r = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for pair in r:
                label_new, string_new = pair
                print(label_new, end='    \r')
                if not int(label_new) == label:
                    if not os.path.isfile(os.path.join(self._root_model, 'counts{}.pkl'.format(label))):
                        if label in top_20:
                            counts = cv.fit_transform(texts)
                            self.clustering(counts, 1, label)
                    texts = []
                label = int(label_new)
                if label in top_20:
                    string = literal_eval(string_new)
                    #tokens = Tokenizer.tokenize(string, 'word')
                    #text = ' '.join([''.join([chr(x) for x in token]) for token in tokens])
                    tokens = Tokenizer.tokenize(string, 'letter')
                    text = ''.join([chr(token) for token in tokens])
                    texts.append(text)
        with open(os.path.join(self._root_model, 'classifier.cv'), 'wb') as f:
            joblib.dump(cv, f)
                               

    def clustering(self, counts, num_clusters, label):
        # km = KMeans(n_clusters=num_clusters)
        # km.fit(counts)
        
        with open(os.path.join(self._root_model, 'counts{}.pkl'.format(label)), 'wb') as f:
            joblib.dump(counts, f)
        #with open(os.path.join(self._root_model, 'cluster{}.pkl'.format(label)), 'wb') as f:
           # joblib.dump(km, f)

    def graph_top_20(self):

        top_20 = ['Python', 'Java', 'JavaScript', 'PHP', 'C#', 'C', 'C++',
                  'R', 'Objective-C', 'Swift', 'Matlab', 'Ruby', 'TypeScript',
                  'Visual Basic', 'Scala', 'Kotlin', 'Go', 'Perl', 'Lua',
                  'Rust', 'Haskell']
        top_20 = [self._languages.index(x) for x in top_20]
        counts = csr_matrix((0, 2 ** 20))
        for label in top_20:
            with open(os.path.join(self._root_model, 'counts{}.pkl'.format(label)), 'rb') as f:
                counts = vstack((counts, joblib.load(f)))
                print(counts.shape)
        if not os.path.isfile(os.path.join(self._root_model, 'dist')):
            dist = euclidean_distances(counts)
            with open(os.path.join(self._root_model, 'dist'), 'wb') as f:
                joblib.dump(dist, f)
        else:
            with open(os.path.join(self._root_model, 'dist'), 'rb') as f:
                dist = joblib.load(f)
            
                
        model = DBSCAN(eps=0.3, min_samples=10)
        print('dbscan fit start')
        db = model.fit(counts)
        print('dbscan fit done')

        labels = db.labels_

        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        
        print('Estimated number of clusters: %d' % n_clusters_)
        

        #fig, ax = plt.subplots(figsize=(15, 550))
        #titles = [self._languages[top_20[x // 500]] for x in list(range(0,counts.shape[0]))]
        #ax = dendrogram(linkage_matrix, orientation="right", labels=titles)

        #plt.tick_params(axis= 'x',
        #                which='both',
        #                bottom=False,
        #                top=False,
        #                labelbottom=False)

        #plt.tight_layout()
        #plt.savefig(os.path.join(self._root_model, 'top_20_dbscan.pdf'))

    def speed_benchmark(self):
        language = [x for x in os.listdir(self._root_training_set) if not x.startswith('.')][10]
        models = self._load_models()

        test_set = self._get_test_set(language)
        total_size = self._count_size(test_set)
        print('{} kB in total'.format(total_size / 1024))
        
        t_start = time.perf_counter()
        self.test_class(models, language)
        t_end = time.perf_counter()
        
        print('{} seconds.'.format(t_end - t_start))
        print('{} seconds per kB'.format(((t_end - t_start) / total_size) * 1024))

    def _count_size(self, files):
        size = 0
        for f in files:
            size += os.path.getsize(f)
        return size

    def file_len(self, fname):
        with open(fname) as f:
            count = 0
            for l in f:
                count += 1
            return count
        
    def _distance(self, model_profile, test_profile):
        distance = 0
        maximum = len(test_profile)

        for test_ngram in test_profile.keys():
            test_rank = test_profile.get(test_ngram)
            model_rank = model_profile.get(test_ngram, maximum)
            d = abs(test_rank - model_rank)
            distance += d

        return distance
    '''    
    def _prob(model, trigrams):
        print('Checking {} model ...'.format(model))
        with open(model, 'rb') as f:
            kneser_ney = load(f)
        result = 1
        for trigram in trigrams:
            prob = kneser_ney.prob(trigram)
            result = result * prob
        return result    
    '''

if __name__ == '__main__':
    main()
